# Proyecto 4
Universidad de Costa Rica
Escuela de Ciencias de la Computacion
Programacion 2 - CI-0113
Grupo 2
Profesor: Esteban Rodriguez Betancourt
Estudiante: Amber Villarreal Campos C28481

# Calculadora

## Instrucciones para compilar
En el directorio raiz del proyecto, ejecuten los siguientes comandos:

```bash
cmake -S . -B build  -DCMAKE_BUILD_TYPE=Debug
cmake --build build
```

Pueden ejecutarlo corriendo el programa que se encuentra en `build/src/Calculadora`:

```bash
./build/src/Calculadora
```

## Instrucciones para correr coverage

En el directorio raiz del proyecto, ejecutar los siguientes comandos:

```bash
cmake -S . -B build  -DCMAKE_BUILD_TYPE=Debug -DCOVERAGE=ON
cmake --build build
cd build
make
make coverage
```

Luego pueden abrir el archivo index.html que se encuentra en la carpeta coverage (build/coverage/index.html).