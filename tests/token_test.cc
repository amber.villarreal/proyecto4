#include <gtest/gtest.h>
#include <token.hh>

TEST(TokenTest, TestInicio)
{
    TokenOperador token(1, 3, '+');
    EXPECT_EQ(token.inicio(), 1);
}

TEST(TokenTest, TestFin)
{
    TokenOperador token(1, 3, '+');
    EXPECT_EQ(token.fin(), 3);
}

TEST(TokenTest, TestPrecedenceMultip)
{
    TokenOperador token(2, 2, '*');
    EXPECT_EQ(token.GetPrecedence(),2);
}

TEST(TokenTest, TestPrecedenceSum)
{
    TokenOperador token(2, 2, '+');
    EXPECT_EQ(token.GetPrecedence(),1);
}

TEST(TokenTest, TestPrecedenceRes)
{
    TokenOperador token(2, 2, '-');
    EXPECT_EQ(token.GetPrecedence(),1);
}

TEST(TokenTest, TestPrecedenceDiv)
{
    TokenOperador token(2, 2, '/');
    EXPECT_EQ(token.GetPrecedence(),2);
}

TEST(TokenTest, TestPrecedenceDefault)
{
    TokenOperador token(2, 2, 't');
    EXPECT_EQ(token.GetPrecedence(),0);
}

TEST(TokenTest, TestTokenIsOperator)
{
    TokenNumero token(2, 2, 2);
    EXPECT_FALSE(token.IsOperator());
}