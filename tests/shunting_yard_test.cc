#include <gtest/gtest.h>
#include <shunting_yard.hh>

class MockTokenizer : public ITokenizer {
private:
    std::queue<Token*> tokens;

public:
    MockTokenizer(const std::vector<Token*>& tokens)
    {
        for (auto token : tokens) {
            this->tokens.push(token);
        }
    }

    virtual bool HasNext() const override
    {
        return !tokens.empty();
    }

    virtual Token* Next() override
    {
        Token* token = tokens.front();
        tokens.pop();
        return token;
    }
};

TEST(ShuntingYardTest, EmptyExpression)
{
    MockTokenizer tokenizer({});
    ShuntingYard shunting_yard(tokenizer);
    std::queue<Token*> postfijo = shunting_yard.ConvertirAPostfijo();

    EXPECT_TRUE(postfijo.empty());
}

TEST(ShuntingYardTest, SumaTest)
{
    //Tokenizer tokenizer("1+1");
    MockTokenizer tokenizer({new TokenNumero(0,0,1),new TokenOperador(1,1,'+'),new TokenNumero(2,2,1)});
    ShuntingYard shunting_yard(tokenizer);
    std::queue<Token*> postfijo = shunting_yard.ConvertirAPostfijo();

    EXPECT_EQ(((TokenNumero*)postfijo.front())->valor(), 1);
    postfijo.pop();
    EXPECT_EQ(((TokenNumero*)postfijo.front())->valor(), 1);
    postfijo.pop();
    EXPECT_EQ(((TokenOperador*)postfijo.front())->operador(), '+');
}

TEST(ShuntingYardTest, ParentesisSuma)
{
    //Tokenizer tokenizer("3*(1+2)");
    MockTokenizer tokenizer({new TokenNumero(0,0,3), new TokenOperador(1,1,'*'), new TokenOperador(2,2,'('), new TokenNumero(3,3,1),new TokenOperador(4,4,'+'),new TokenNumero(5,5,2), new TokenOperador(6,6,')')});
    ShuntingYard shunting_yard(tokenizer);
    std::queue<Token*> postfijo = shunting_yard.ConvertirAPostfijo();

    EXPECT_EQ(((TokenNumero*)postfijo.front())->valor(), 3);
    postfijo.pop();
    EXPECT_EQ(((TokenNumero*)postfijo.front())->valor(), 1);
    postfijo.pop();
    EXPECT_EQ(((TokenNumero*)postfijo.front())->valor(), 2);
    postfijo.pop();
    EXPECT_EQ(((TokenOperador*)postfijo.front())->operador(), '+');
    postfijo.pop();
    EXPECT_EQ(((TokenOperador*)postfijo.front())->operador(), '*');
}

TEST(ShuntingYardTest, OperadoresPrioridad)
{
    //Tokenizer tokenizer("3*1+2");
    MockTokenizer tokenizer({new TokenNumero(0,0,3), new TokenOperador(1,1,'*'), new TokenNumero(2,2,1),new TokenOperador(3,3,'+'),new TokenNumero(4,4,2)});
    ShuntingYard shunting_yard(tokenizer);
    std::queue<Token*> postfijo = shunting_yard.ConvertirAPostfijo();

    EXPECT_EQ(((TokenNumero*)postfijo.front())->valor(), 3);
    postfijo.pop();
    EXPECT_EQ(((TokenNumero*)postfijo.front())->valor(), 1);
    postfijo.pop();
    EXPECT_EQ(((TokenOperador*)postfijo.front())->operador(), '*');
    postfijo.pop();
    EXPECT_EQ(((TokenNumero*)postfijo.front())->valor(), 2);
    postfijo.pop();
    EXPECT_EQ(((TokenOperador*)postfijo.front())->operador(), '+');
}