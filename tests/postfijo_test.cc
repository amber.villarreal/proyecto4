#include <postfijo.hh>
#include <gtest/gtest.h>

TEST(PostfijoTest, ReturnResultado)
{
    std::queue<Token*> cola;
    cola.push(new TokenNumero(0, 0, 5));
    Postfijo postfijo(cola);
    EXPECT_EQ(postfijo.Resultado(), 5);
}

TEST(PostfijoTest, Suma)
{
    std::queue<Token*> cola;
    cola.push(new TokenNumero(0, 0, 4));
    cola.push(new TokenNumero(2, 2, 3));
    cola.push(new TokenOperador(1, 1, '+'));
    Postfijo postfijo(cola);
    EXPECT_EQ(postfijo.Resultado(), 7);
}

TEST(PostfijoTest, Resta)
{
    std::queue<Token*> cola;
    cola.push(new TokenNumero(0, 0, 4));
    cola.push(new TokenNumero(2, 2, 3));
    cola.push(new TokenOperador(1, 1, '-'));
    Postfijo postfijo(cola);
    EXPECT_EQ(postfijo.Resultado(), 1);
}

TEST(PostfijoTest, Multiplicacion)
{
    std::queue<Token*> cola;
    cola.push(new TokenNumero(0, 0, 4));
    cola.push(new TokenNumero(2, 2, 3));
    cola.push(new TokenOperador(1, 1, '*'));
    Postfijo postfijo(cola);
    EXPECT_EQ(postfijo.Resultado(), 12);
}

TEST(PostfijoTest, Division)
{
        std::queue<Token*> cola;
    cola.push(new TokenNumero(0, 0, 4));
    cola.push(new TokenNumero(2, 2, 2));
    cola.push(new TokenOperador(1, 1, '/'));
    Postfijo postfijo(cola);
    EXPECT_EQ(postfijo.Resultado(), 2);
}

TEST(PostfijoTest, DivisionCero)
{
    std::queue<Token*> cola;
    cola.push(new TokenNumero(0, 0, 4));
    cola.push(new TokenNumero(2, 2, 0));
    cola.push(new TokenOperador(1, 1, '/'));
    Postfijo postfijo(cola);
    EXPECT_FALSE(postfijo.IsValid());
}

TEST(PostfijoTest, OperandosSobrantes)
{
    std::queue<Token*> cola;
    cola.push(new TokenNumero(0, 0, 4));
    cola.push(new TokenNumero(2, 2, 4));
    cola.push(new TokenOperador(1, 1, '*'));
    cola.push(new TokenNumero(2, 2, 4));
    Postfijo postfijo(cola);
    EXPECT_EQ(postfijo.Error(), "Error: sobran operandos");
}

TEST(PostfijoTest, OperandosFaltantes)
{
    std::queue<Token*> cola;
    cola.push(new TokenNumero(0, 0, 4));
    cola.push(new TokenOperador(1, 1, '*'));
    Postfijo postfijo(cola);
    EXPECT_EQ(postfijo.Error(),"Error: faltan operandos");
}