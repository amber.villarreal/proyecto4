#include <calculadora.hh>
#include <gtest/gtest.h>
#include <iostream>
#include <string>

TEST(CalculadoraTest, EmptyExpression)
{
    Calculadora calculadora("");
    EXPECT_TRUE(calculadora.IsValid());
    EXPECT_EQ(calculadora.Resultado(), 0);
}

// Revisamos que la calculadora si estuviera detectando el error
TEST(CalculadoraTest, InvalidInput)
{
    Calculadora calculadora("test");
    EXPECT_FALSE(calculadora.IsValid());
    EXPECT_NE(calculadora.Error(), "");
}

// Revisamos que tenga el retorno correcto de "salir" 
TEST(CalculadoraTest, InputSalida)
{
    std::istringstream is("salir");
    EXPECT_FALSE(ask_user(is, std::cout));
}

// Revisamos OUTPUT de error
TEST(CalculadoraTest, OutputError)
{
    std::ostringstream text;
    std::istringstream in("test");
    ask_user(in, text);
    EXPECT_EQ(text.str(), "Ingrese una expresión o escriba \"salir\": Error: Número mal formado: se encontró un caracter inválido\n");
}

// Revisamos OUTPUT correcto 
TEST(CalculadoraTest, OutputResultado)
{
    std::ostringstream text;
    std::istringstream in("0+0");
    ask_user(in, text);
    EXPECT_EQ(text.str(), "Ingrese una expresión o escriba \"salir\": El resultado es: 0\n");
}
