
#include <gtest/gtest.h>
#include <tokenizer.hh>

TEST(TokenizerTest, EmptyString)
{
    Tokenizer tokenizer("");
    EXPECT_FALSE(tokenizer.HasNext());
}

TEST(TokenizerTest, TestPosition)
{
    Tokenizer tokenizer(" 0");
    tokenizer.Next();
    EXPECT_EQ(tokenizer.GetPosition(), 2);
}

TEST(TokenizerTest, TestNullPosition)
{
    Tokenizer tokenizer("");
    EXPECT_EQ(tokenizer.Next(), nullptr);
}

TEST(TokenizerTest, TestDecimalTrue)
{
    Tokenizer tokenizer("0.0");
    Token *valor = tokenizer.Next();
    EXPECT_EQ(((TokenNumero*)valor)->valor(), 0);
}