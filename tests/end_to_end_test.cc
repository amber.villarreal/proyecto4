#include <calculadora.hh>
#include <gtest/gtest.h>

//Serevisan distintas operaciones y operaciones combinadas
TEST(CalculadoraTest, Operaciones)
{
    std::ostringstream textI;
    std::istringstream inI("1+2");
    ask_user(inI, textI);
    EXPECT_EQ(textI.str(), "Ingrese una expresión o escriba \"salir\": El resultado es: 3\n");

    std::ostringstream textII;
    std::istringstream inII("1-2");
    ask_user(inII, textII);
    EXPECT_EQ(textII.str(), "Ingrese una expresión o escriba \"salir\": El resultado es: -1\n");

    std::ostringstream textIII;
    std::istringstream inIII("1*2");
    ask_user(inIII, textIII);
    EXPECT_EQ(textIII.str(), "Ingrese una expresión o escriba \"salir\": El resultado es: 2\n");

    std::ostringstream textIV;
    std::istringstream inIV("1/2");
    ask_user(inIV, textIV);
    EXPECT_EQ(textIV.str(), "Ingrese una expresión o escriba \"salir\": El resultado es: 0.5\n");

    std::ostringstream textV;
    std::istringstream inV("1+3*2-5+4/2");
    ask_user(inV, textV);
    EXPECT_EQ(textV.str(), "Ingrese una expresión o escriba \"salir\": El resultado es: 4\n");

    std::ostringstream textVI;
    std::istringstream inVI("(1+3)*2-(5+4)/2");
    ask_user(inVI, textVI);
    EXPECT_EQ(textVI.str(), "Ingrese una expresión o escriba \"salir\": El resultado es: 3.5\n");
}

// Se revisan entradas que no son operaciones, salir, entrada invalida, e operaciones incompletas
TEST(CalculadoraTest, EntradasMiscelaneas)
{
    std::ostringstream textI;
    std::istringstream inI("test");
    ask_user(inI, textI);
    EXPECT_EQ(textI.str(), "Ingrese una expresión o escriba \"salir\": Error: Número mal formado: se encontró un caracter inválido\n");

    std::ostringstream textII;
    std::istringstream inII("salir");
    ask_user(inII, textII);
    EXPECT_EQ(textII.str(), "Ingrese una expresión o escriba \"salir\": Saliendo...\n");

    std::ostringstream textIII;
    std::istringstream inIII("1*");
    ask_user(inIII, textIII);
    EXPECT_EQ(textIII.str(), "Ingrese una expresión o escriba \"salir\": El resultado es: 0\n");

    std::ostringstream textIV;
    std::istringstream inIV("1 ++ + 4");
    ask_user(inIV, textIV);
    EXPECT_EQ(textIV.str(), "Ingrese una expresión o escriba \"salir\": El resultado es: 0\n");
}